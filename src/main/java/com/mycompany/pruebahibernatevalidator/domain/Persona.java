package com.mycompany.pruebahibernatevalidator.domain;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Emiliano
 */
public class Persona {

    @NotNull
    private String nombre;
    @NotNull
    @Pattern(regexp = "[0-9][0-9].[0-9][0-9][0-9].[0-9][0-9][0-9]")
    private String documento;
    @Min(value = 18, message = "Tenés que ser mayor de edad.")
    private int edad;
    @Valid
    private Automovil automovil;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the automovil
     */
    public Automovil getAutomovil() {
        return automovil;
    }

    /**
     * @param automovil the automovil to set
     */
    public void setAutomovil(Automovil automovil) {
        this.automovil = automovil;
    }
}
