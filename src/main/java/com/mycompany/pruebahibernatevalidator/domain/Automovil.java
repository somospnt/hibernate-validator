package com.mycompany.pruebahibernatevalidator.domain;

import org.hibernate.validator.constraints.Range;

/**
 *
 * @author Emiliano
 */
public class Automovil {

    @Range(min = 4, max = 4, message = "El auto tiene que tener 4 ruedas capo")
    private int cantidadRuedas;
//    @Vtv
    private boolean pasoVerificacionTecnica;

    /**
     * @return the cantidadRuedas
     */
    public int getCantidadRuedas() {
        return cantidadRuedas;
    }

    /**
     * @param cantidadRuedas the cantidadRuedas to set
     */
    public void setCantidadRuedas(int cantidadRuedas) {
        this.cantidadRuedas = cantidadRuedas;
    }

    /**
     * @return the tieneMotor
     */
    public boolean isPasoVerificacionTecnica() {
        return pasoVerificacionTecnica;
    }

    /**
     * @param tieneMotor the tieneMotor to set
     */
    public void setPasoVerificacionTecnica(boolean tieneMotor) {
        this.pasoVerificacionTecnica = tieneMotor;
    }
}
