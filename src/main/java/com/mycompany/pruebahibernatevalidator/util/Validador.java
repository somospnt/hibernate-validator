package com.mycompany.pruebahibernatevalidator.util;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * La clase Validador implementa validacion de entidades usando Hibernate
 * Validator. Para mas info:
 * http://hibernate.org/validator/documentation/getting-started/
 *
 * @author Emiliano
 */
public class Validador {

    private static Validator validator;

    /**
     * Ejecuta todas las validaciones de la entidad que recibe como parametro.
     * Dichas validaciones deben ser puestas mediante anotaciones en la clase de
     * la entidad.
     *
     * @param entidad a validar.
     * @return true si la entidad cumple con todas las validaciones.
     */
    public static boolean puedoManejar(Object entidad) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(entidad);
        for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
            System.out.println(constraintViolation.getMessage());
        }
        return constraintViolations.isEmpty();
    }

}
