package com.mycompany.pruebahibernatevalidator.util;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Emiliano
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = VerificacionTecnica.class)
@Documented
public @interface Vtv {

    String message() default "No paso la vtv";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
