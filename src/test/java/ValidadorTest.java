
import com.mycompany.pruebahibernatevalidator.domain.Automovil;
import com.mycompany.pruebahibernatevalidator.util.Validador;
import com.mycompany.pruebahibernatevalidator.domain.Persona;
import org.junit.Assert;
import org.junit.Test;

/**
 * Clase de test para la clase Validador.
 *
 * Supongamos que tenemos las clases Automovil y Persona. Una persona puede
 * manejar si tiene nombre, documento, su documento cumple con el formato
 * nn.nnn.nnn y es mayor de edad Un auto puede manejarse si posee 4 ruedas.
 *
 * Queremos tener un método que valide si una persona puede manejar, y si un
 * automovil puede ser manejado. A efectos demostrativos, vamos a usar el mismo
 * método para ambas entidades. Para implementar estas validaciones, anotaremos
 * las clases de dominio, y vamos a crear una clase validador que las ejecute.
 *
 * Para probar el funcionamiento de nuestro código crearemos la clase
 * ValidadorTest, en donde iremos poniendo cada uno de los casos.
 *
 * @author Emiliano
 */
public class ValidadorTest {

    /**
     * Vamos a probar un metodo llamado puedoManejar. Una persona puede manejar
     * si tiene nombre, documento, su documento cumple con el formato nn.nnn.nnn
     * y es mayor de edad.
     */
    @Test
    public void validar_conPersonaCorrecta_retornaTrue() {
        Persona persona = new Persona();
        persona.setNombre("Emiliano");
        persona.setDocumento("12.345.678");
        persona.setEdad(27);

        Assert.assertTrue(Validador.puedoManejar(persona));
    }

    @Test
    public void validar_conPersonaNombreNull_retornaFalse() {
        Persona persona = new Persona();
        persona.setDocumento("12.345.678");
        persona.setEdad(27);

        Assert.assertFalse(Validador.puedoManejar(persona));
    }

    @Test
    public void validar_conPersonaDniNull_retornaFalse() {
        Persona persona = new Persona();
        persona.setNombre("Emiliano");
        persona.setEdad(27);

        Assert.assertFalse(Validador.puedoManejar(persona));
    }

    @Test
    public void validar_conPersonaDniMalFormato_retornaFalse() {
        Persona persona = new Persona();
        persona.setNombre("Emiliano");
        persona.setDocumento("33709.718");
        persona.setEdad(27);

        Assert.assertFalse(Validador.puedoManejar(persona));
    }

    @Test
    public void validar_conPersonaMenorEdad_retornaFalse() {
        Persona persona = new Persona();
        persona.setNombre("Emiliano");
        persona.setDocumento("12.345.678");
        persona.setEdad(17);

        Assert.assertFalse(Validador.puedoManejar(persona));
    }

    /**
     * Ahora quiero usar el mismo metodo puedoManejar para ver si puedo manejar
     * un auto. Para poder manejar un auto, el auto tiene que tener 4 ruedas.
     */
    @Test
    public void validar_conAutoConDistintasCantidadRuedas_retornaFalse() {
        Automovil automovil = new Automovil();
        automovil.setPasoVerificacionTecnica(true);
        automovil.setCantidadRuedas(3);

        Assert.assertFalse(Validador.puedoManejar(automovil));
    }

    /**
     * Para que una persona pueda manejar, si tiene auto, su auto tambien debe
     * cumplir con las validaciones.
     */
    @Test
    public void validar_conPersonaCorrectaYAutomovilIncorrecto_retornaFalse() {
        Automovil automovil = new Automovil();
        automovil.setPasoVerificacionTecnica(true);
        automovil.setCantidadRuedas(3);

        Persona persona = new Persona();
        persona.setNombre("Emiliano");
        persona.setDocumento("12.345.678");
        persona.setEdad(27);
        persona.setAutomovil(automovil);

        Assert.assertFalse(Validador.puedoManejar(persona));
    }

//    @Test
//    public void validar_conPersonaCorrectaYAutomovilSinMotor_retornaFalse() {
//        Automovil automovil = new Automovil();
//        automovil.setPasoVerificacionTecnica(false);
//        automovil.setCantidadRuedas(4);
//
//        Persona persona = new Persona();
//        persona.setNombre("Emiliano");
//        persona.setDocumento("12.345.678");
//        persona.setEdad(27);
//        persona.setAutomovil(automovil);
//
//        Assert.assertFalse(Validador.puedoManejar(persona));
//    }
}
